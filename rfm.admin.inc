<?php

/**
 * @file
 * Admin page callbacks of Responsivefilemanager module
 */

/**
 * Form callback; RF main config form.
 *
 * @param $form
 *   An associative array containing the structure of the form
 * @return
 *   An array representing the form definition
 * @ingroup forms
 *
 * @see rfm_settings_form_submit()
 */
function rfm_settings_form($form) {

  $true_false_options = array(
    0 => t('No'),
    1 => t('Yes'),
  );
  //*******************************************
  // Main settings
  //*******************************************
  $form['rfm_main_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Main settings'),
  );

  $form['rfm_main_settings']['rfm_upload_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Upload dir'),
    '#default_value' => variable_get('rfm_upload_dir', 'source'),
    '#description' => t('Name of upload folder')
  );
  $form['rfm_main_settings']['rfm_thumbs_base_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Thumbs dir'),
    '#default_value' => variable_get('rfm_thumbs_base_path', 'thumbs'),
    '#description' => t('Name of thumbs folder'),
  );
  $form['rfm_main_settings']['rfm_access_keys_salt'] = array(
    '#type' => 'textfield',
    '#title' => t('Access keys salt'),
    '#default_value' => variable_get('rfm_access_keys_salt', 'z58srVoADqLhD7eW2'),
    '#description' => t('This salt uses for generating access keys'),
  );
  $form['rfm_main_settings']['rfm_max_size_total'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum size of files'),
    '#default_value' => variable_get('rfm_max_size_total', 0),
    '#description' => t('Maximum size of all files in source folder (in Megabytes). 0 - no restrictions'),
  );
  $form['rfm_main_settings']['rfm_max_size_upload'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum upload size'),
    '#default_value' => variable_get('rfm_max_size_upload', 1),
    '#description' => t('Maximum upload size (in Megabytes)'),
  );
  $form['rfm_main_settings']['rfm_language_default'] = array(
    '#type' => 'select',
    '#title' => t('Default language'),
    '#options' => options_language_select(),
    '#default_value' => variable_get('rfm_language_default', get_default_language()),
  );
  $form['rfm_main_settings']['rfm_icon_theme'] = array(
    '#type' => 'select',
    '#title' => t('Icon theme'),
    '#options' => array(
      'ico' => t('ico'),
      'ico_dark' => t('ico_dark')
    ),
    '#default_value' => variable_get('rfm_icon_theme', 'ico'),
  );
  $form['rfm_main_settings']['rfm_show_total_size'] = array(
    '#type' => 'radios',
    '#title' => t('Show total size'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_show_total_size', 0),
    '#description' => t('Show or not total size in filemanager (is possible to greatly increase the calculations)'),
  );
  $form['rfm_main_settings']['rfm_show_folder_size'] = array(
    '#type' => 'radios',
    '#title' => t('Show folder size'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_show_folder_size', 0),
    '#description' => t('Show or not show folder size in list view feature in filemanager (is possible, if there is a large folder, to greatly increase the calculations)'),
  );
  $form['rfm_main_settings']['rfm_show_sorting_bar'] = array(
    '#type' => 'radios',
    '#title' => t('Show sorting bar'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_show_sorting_bar', 1),
    '#description' => t('Show or not show sorting feature in filemanager'),
  );
  $form['rfm_main_settings']['rfm_show_filter_buttons'] = array(
    '#type' => 'radios',
    '#title' => t('Show filter buttons'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_show_filter_buttons', 1),
    '#description' => t('Show or not show filters button in filemanager'),
  );
  $form['rfm_main_settings']['rfm_show_language_selection'] = array(
    '#type' => 'radios',
    '#title' => t('Show language selection'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_show_language_selection', 0),
    '#description' => t('Show or not language selection feature in filemanager'),
  );
  $form['rfm_main_settings']['rfm_transliteration'] = array(
    '#type' => 'radios',
    '#title' => t('Transliteration'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_transliteration', 0),
    '#description' => t('active or deactive the transliteration (mean convert all strange characters in A..Za..z0..9 characters)'),
  );
  $form['rfm_main_settings']['rfm_convert_spaces'] = array(
    '#type' => 'radios',
    '#title' => t('Convert spaces'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_convert_spaces', 0),
    '#description' => t('convert all spaces on files name and folders name with "replace_with" variable'),
  );
  $form['rfm_main_settings']['rfm_replace_with'] = array(
    '#type' => 'textfield',
    '#title' => t('Replace with'),
    '#default_value' => variable_get('rfm_replace_with', "_"),
    '#description' => t('convert all spaces on files name and folders name this value'),
  );
  $form['rfm_main_settings']['rfm_lower_case'] = array(
    '#type' => 'radios',
    '#title' => t('Lower case'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_lower_case', 0),
    '#description' => t('convert to lowercase the files and folders name'),
  );
  $form['rfm_main_settings']['rfm_lazy_loading_file_number_threshold'] = array(
    '#type' => 'textfield',
    '#title' => t('Lazy loading file number threshold'),
    '#default_value' => variable_get('rfm_lazy_loading_file_number_threshold', 0),
    '#description' => t('-1: There is no lazy loading at all, 0: Always lazy-load images, 0+: The minimum number of the files in a directory when lazy loading should be turned on.'),
  );
  //*******************************************
  //Images limit and resizing configuration
  //*******************************************
  $form['rfm_images_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Images limit and resizing configuration'),
    '#description' => t('Set maximum pixel width and/or maximum pixel height for all images. If you set a maximum width or height, oversized images are converted to those limits. Images smaller than the limit(s) are unaffected.
    if you don\'t need a limit set both to 0'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rfm_images_settings']['rfm_image_max_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Image max width'),
    '#default_value' => variable_get('rfm_image_max_width', 0),
  );
  $form['rfm_images_settings']['rfm_image_max_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Image max height'),
    '#default_value' => variable_get('rfm_image_max_height', 0),
  );

  $image_resizing_options = array(
    'exact' => 'defined size',
    'portrait' => 'keep aspect set height',
    'landscape' => 'keep aspect set width',
    'auto' => 'auto',
    'crop' => 'resize and crop',
  );

  $form['rfm_images_settings']['rfm_image_max_mode'] = array(
    '#type' => 'select',
    '#title' => t('Image max mode'),
    '#default_value' => variable_get('rfm_image_max_mode', 'auto'),
    '#options' => $image_resizing_options,
  );
  //Automatic resizing //
  $form['rfm_images_settings']['rfm_image_resizing'] = array(
    '#type' => 'radios',
    '#title' => t('Image resizing'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_image_resizing', 0),
    '#description' => t('If you set "Image resizing" to YES the script converts all uploaded images exactly to "Image resizing width" x "Image resizing height" dimension. \n
    If you set width or height to 0 the script automatically calculates the other dimension. \n
    Is possible that if you upload very big images the script not work to overcome this increase the php configuration of memory and time limit
    '),
  );
  $form['rfm_images_settings']['rfm_image_resizing_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Image resizing width'),
    '#default_value' => variable_get('rfm_image_resizing_width', 0),
  );
  $form['rfm_images_settings']['rfm_image_resizing_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Image resizing height'),
    '#default_value' => variable_get('rfm_image_resizing_height', 0),
  );
  $form['rfm_images_settings']['rfm_image_resizing_mode'] = array(
    '#type' => 'select',
    '#title' => t('Image resizing mode'),
    '#default_value' => variable_get('rfm_image_resizing_mode', 'auto'),
    '#options' => $image_resizing_options,
  );
  $form['rfm_images_settings']['rfm_image_resizing_override'] = array(
    '#type' => 'radios',
    '#title' => t('Image resizing override'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_image_resizing_override', 0),
    '#description' => t('If set to TRUE then you can specify bigger images than "Image max width/height)" otherwise if "Image resizing" is bigger than "Image max width" or height then it will be converted to those values'),
  );

  //******************
  // Default layout setting
  //******************
  $form['rfm_layout_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default layout setting'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $default_layout_setting = array(
    0 => "boxes",
    1 => "detailed list (1 column)",
    2 => "columns list (multiple columns depending on the width of the page)",
  );
  $form['rfm_layout_settings']['rfm_default_view'] = array(
    '#type' => 'select',
    '#title' => t('Default view'),
    '#options' => $default_layout_setting,
    '#default_value' => variable_get('rfm_default_view', 0),
  );
  //*************************
  //Permissions configuration
  //******************
  $form['rfm_permissions_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Permissions configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rfm_permissions_config']['rfm_delete_files'] = array(
    '#type' => 'radios',
    '#title' => t('Delete files'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_delete_files', 1),
  );
  $form['rfm_permissions_config']['rfm_create_folders'] = array(
    '#type' => 'radios',
    '#title' => t('Create folders'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_create_folders', 1),
  );
  $form['rfm_permissions_config']['rfm_delete_folders'] = array(
    '#type' => 'radios',
    '#title' => t('Delete folders'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_delete_folders', 1),
  );
  $form['rfm_permissions_config']['rfm_upload_files'] = array(
    '#type' => 'radios',
    '#title' => t('Upload files'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_upload_files', 1),
  );
  $form['rfm_permissions_config']['rfm_rename_files'] = array(
    '#type' => 'radios',
    '#title' => t('Rename files'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_rename_files', 1),
  );
  $form['rfm_permissions_config']['rfm_rename_folders'] = array(
    '#type' => 'radios',
    '#title' => t('Rename folders'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_rename_folders', 1),
  );
  $form['rfm_permissions_config']['rfm_duplicate_files'] = array(
    '#type' => 'radios',
    '#title' => t('Duplicate files'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_duplicate_files', 1),
  );
  $form['rfm_permissions_config']['rfm_copy_cut_files'] = array(
    '#type' => 'radios',
    '#title' => t('Copy/cut files'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_copy_cut_files', 1),
    '#description' => t('for copy/cut files'),
  );
  $form['rfm_permissions_config']['rfm_copy_cut_dirs'] = array(
    '#type' => 'radios',
    '#title' => t('Copy/cut dirs'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_copy_cut_dirs', 1),
    '#description' => t('for copy/cut directories'),
  );
  $form['rfm_permissions_config']['rfm_chmod_files'] = array(
    '#type' => 'radios',
    '#title' => t('Chmod files'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_chmod_files', 1),
    '#description' => t('change file permissions'),
  );
  $form['rfm_permissions_config']['rfm_chmod_dirs'] = array(
    '#type' => 'radios',
    '#title' => t('Chmod dirs'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_chmod_dirs', 1),
    '#description' => t('change folder permissions'),
  );
  $form['rfm_permissions_config']['rfm_preview_text_files'] = array(
    '#type' => 'radios',
    '#title' => t('Preview text files'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_preview_text_files', 1),
    '#description' => t('eg.: txt, log etc.'),
  );
  $form['rfm_permissions_config']['rfm_edit_text_files'] = array(
    '#type' => 'radios',
    '#title' => t('Edit text files'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_edit_text_files', 1),
    '#description' => t('eg.: txt, log etc.'),
  );
  $form['rfm_permissions_config']['rfm_create_text_files'] = array(
    '#type' => 'radios',
    '#title' => t('Create text files'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_create_text_files', 1),
    '#description' => t('only create files with exts. defined in "Editable text file exts"'),
  );
  $form['rfm_permissions_config']['rfm_previewable_text_file_exts'] = array(
    '#type' => 'textfield',
    '#title' => t('Preview type of files'),
    '#default_value' => variable_get('rfm_previewable_text_file_exts', "txt log xml html css htm js"),
    '#description' => 'you can preview these type of files if "Preview text files" is true'
  );
  $form['rfm_permissions_config']['rfm_previewable_text_file_exts_no_prettify'] = array(
    '#type' => 'textfield',
    '#title' => t('No prettify preview these type of files'),
    '#default_value' => variable_get('rfm_previewable_text_file_exts_no_prettify', "txt log"),
  );
  $form['rfm_permissions_config']['rfm_editable_text_file_exts'] = array(
    '#type' => 'textfield',
    '#title' => t('Editable text file exts'),
    '#default_value' => variable_get('rfm_editable_text_file_exts', "txt log xml html css htm js"),
  );
  $form['rfm_permissions_config']['rfm_googledoc_enabled'] = array(
    '#type' => 'radios',
    '#title' => t('Preview with Google Documents'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_googledoc_enabled', 1),
  );
  $form['rfm_permissions_config']['rfm_googledoc_file_exts'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Documents file exts'),
    '#default_value' => variable_get('rfm_googledoc_file_exts', "doc docx xls xlsx ppt pptx"),
  );
  $form['rfm_permissions_config']['rfm_viewerjs_file_exts'] = array(
    '#type' => 'radios',
    '#title' => t('Preview with Viewer.js'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_viewerjs_file_exts', 1),
  );
  $form['rfm_permissions_config']['rfm_viewerjs_file_exts'] = array(
    '#type' => 'textfield',
    '#title' => t('Viewer.js file exts'),
    '#default_value' => variable_get('rfm_viewerjs_file_exts', "pdf odt odp ods"),
  );
  $form['rfm_permissions_config']['rfm_copy_cut_max_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Size limit for paste'),
    '#default_value' => variable_get('rfm_copy_cut_max_size', 100),
    '#description' => t("defines size limit for paste in MB / operation (set '0' for no limit)"),
  );
  $form['rfm_permissions_config']['rfm_copy_cut_max_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Count limit for paste'),
    '#default_value' => variable_get('rfm_copy_cut_max_count', 200),
    '#description' => t("defines file count limit for paste / operation (set '0' for no limit)"),
  );
  //**********************
  //Allowed extensions (lowercase insert)
  //**********************
  $form['rfm_allowed_extensions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Allowed extensions'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rfm_allowed_extensions']['rfm_ext_img'] = array(
    '#type' => 'textfield',
    '#title' => t('Image exts'),
    '#default_value' => variable_get('rfm_ext_img', 'jpg jpeg png gif bmp tiff svg'),
  );
  $form['rfm_allowed_extensions']['rfm_ext_file'] = array(
    '#type' => 'textfield',
    '#title' => t('File exts'),
    '#maxlength' => 256,
    '#default_value' => variable_get('rfm_ext_file', 'doc docx rtf pdf xls xlsx txt csv html xhtml psd sql log fla xml ade adp mdb accdb ppt pptx odt ots ott odb odg otp otg odf ods odp css ai'),
  );
  $form['rfm_allowed_extensions']['rfm_ext_video'] = array(
    '#type' => 'textfield',
    '#title' => t('Video exts'),
    '#default_value' => variable_get('rfm_ext_video', 'mov mpeg m4v mp4 avi mpg wma flv webm'),
  );
  $form['rfm_allowed_extensions']['rfm_ext_music'] = array(
    '#type' => 'textfield',
    '#title' => t('Music exts'),
    '#default_value' => variable_get('rfm_ext_music', 'mp3 m4a ac3 aiff mid ogg wav'),
  );
  $form['rfm_allowed_extensions']['rfm_ext_misc'] = array(
    '#type' => 'textfield',
    '#title' => t('Archives exts'),
    '#default_value' => variable_get('rfm_ext_misc', 'zip rar gz tar iso dmg'),
  );
  /******************
   * AVIARY config
   *******************/
  $form['rfm_aviary_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Aviary config'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rfm_aviary_config']['rfm_aviary_active'] = array(
    '#type' => 'radios',
    '#title' => t('Aviary active'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_aviary_active', 1),
  );
  $form['rfm_aviary_config']['rfm_aviary_apiKey'] = array(
    '#type' => 'textfield',
    '#title' => t('Aviary apiKey'),
    '#default_value' => variable_get('rfm_aviary_apiKey', '2444282ef4344e3dacdedc7a78f8877d'),
  );
  $form['rfm_aviary_config']['rfm_aviary_language'] = array(
    '#type' => 'select',
    '#title' => t('Aviary language'),
    '#options' => options_language_select(),
    '#default_value' => variable_get('rfm_aviary_language', get_default_language()),
  );
  $form['rfm_aviary_config']['rfm_aviary_theme'] = array(
    '#type' => 'textfield',
    '#title' => t('Aviary theme'),
    '#default_value' => variable_get('rfm_aviary_theme', 'light'),
  );
  $form['rfm_aviary_config']['rfm_aviary_tools'] = array(
    '#type' => 'textfield',
    '#title' => t('Aviary tools'),
    '#default_value' => variable_get('rfm_aviary_tools', 'all'),
  );
  $form['rfm_aviary_config']['rfm_aviary_maxsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Aviary max size'),
    '#default_value' => variable_get('rfm_aviary_maxsize', '1400'),
  );
  $form['rfm_aviary_config']['rfm_file_number_limit_js'] = array(
    '#type' => 'textfield',
    '#title' => t('Aviary files number limit'),
    '#default_value' => variable_get('rfm_file_number_limit_js', 500),
  );
  //**********************
  // Hidden files and folders
  //**********************
  $form['rfm_fs_hidden_files'] = array(
    '#type' => 'fieldset',
    '#title' => t('Hidden files and folders'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rfm_fs_hidden_files']['rfm_hidden_folders'] = array(
    '#type' => 'textfield',
    '#title' => t('Hidden folders'),
    '#default_value' => variable_get('rfm_hidden_folders', ''),
  );
  $form['rfm_fs_hidden_files']['rfm_hidden_files'] = array(
    '#type' => 'textfield',
    '#title' => t('Hidden files'),
    '#default_value' => variable_get('rfm_hidden_files', ''),
  );
  /*******************
   * JAVA upload
   *******************/
  $form['rfm_fs_java_upload'] = array(
    '#type' => 'fieldset',
    '#title' => t('Java upload'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rfm_fs_java_upload']['rfm_java_upload'] = array(
    '#type' => 'radios',
    '#title' => t('Java upload'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_java_upload', 1),
  );
  $form['rfm_fs_java_upload']['rfm_java_max_size_upload'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum file size for upload'),
    '#default_value' => variable_get('rfm_java_max_size_upload', 200),
    '#description' => t('Maximum file size for upload (Gb)')
  );
  //**********************
  // Other settings
  //**********************
  $form['rfm_other_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['rfm_other_settings']['rfm_use_access_keys'] = array(
    '#type' => 'radios',
    '#title' => t('Use access keys'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_use_access_keys', 0),
    '#description' => t('if set to true only those will access RF whose url contains the access key(akey). /n
      In tinymce a new parameter added: filemanager_access_key:"myPrivateKey"'),
  );
  $form['rfm_other_settings']['rfm_debug_error_message'] = array(
    '#type' => 'radios',
    '#title' => t('Debug error message'),
    '#options' => $true_false_options,
    '#default_value' => variable_get('rfm_debug_error_message', 1),
  );
  rfm_config();
  return system_settings_form($form);
}


/**
 * Return language list for select options
 */
function options_language_select() {
  return array(
    'az_AZ' => 'Azərbaycan dili',
    'bg_BG' => 'български език',
    'ca' => 'Català, valencià',
    'cs' => 'čeština, český jazyk',
    'da' => 'Dansk',
    'de' => 'Deutsch',
    'el_GR' => 'ελληνικά',
    'en_EN' => 'English',
    'es' => 'Español',
    'fa' => 'فارسی',
    'fr_FR' => 'Français',
    'he' => 'עברית',
    'hr' => 'Hrvatski jezik',
    'hu_HU' => 'Magyar',
    'id' => 'Bahasa Indonesia',
    'he_IL' => 'Hebrew (Israel)',
    'it' => 'Italiano',
    'ja' => '日本',
    'lt' => 'Lietuvių kalba',
    'mn_MN' => 'монгол',
    'nb_NO' => 'Norsk bokmål',
    'nl' => 'Nederlands, Vlaams',
    'pl' => 'Język polski, polszczyzna',
    'pt_BR' => 'Português(Brazil)',
    'pt_PT' => 'Português',
    'ru' => 'Pусский язык',
    'sk' => 'Slovenčina',
    'sl' => 'Slovenski jezik',
    'sv_SE' => 'Svenska',
    'tr_TR' => 'Türkçe',
    'uk_UA' => 'Yкраїнська мова',
    'vi' => 'Tiếng Việt',
    'zh_CN' => '中文 (Zhōngwén), 汉语, 漢語',
    // source: http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
  );
}

/**
 * Return default site language
 */
function get_default_language() {
  $defaultLanguage = language_default();
  $defaultLanguage = $defaultLanguage->language;
  $lang_name_mapping = array(
    'az' => 'az_AZ',
    'en' => 'en_EN',
    'bg' => 'bg_BG',
    'ca' => 'ca',
    'cs' => 'cs',
    'da' => 'da',
    'de' => 'de',
    'el' => 'el_GR',
    'en' => 'en_EN',
    'es' => 'es',
    'fa' => 'fa',
    'fr' => 'fr_FR',
    'hr' => 'hr',
    'hu' => 'hu_HU',
    'id' => 'id',
    'he' => 'he_IL',
    'it' => 'it',
    'ja' => 'ja',
    'lt' => 'lt',
    'mn' => 'mn_MN',
    'nb' => 'nb_NO',
    'nl' => 'nl',
    'pl' => 'pl',
    'pt-br' => 'pt_BR',
    'pt-pt' => 'pt_PT',
    'ru' => 'ru',
    'sk' => 'sk',
    'sl' => 'sl',
    'sv' => 'sv_SE',
    'tr' => 'tr_TR',
    'uk' => 'uk_UA',
    'vi' => 'vi',
    'zh-hant' => 'zh_CN',
    'zh-hans' => 'zh_CN',
  );

  return isset($lang_name_mapping[$defaultLanguage]) ? $lang_name_mapping[$defaultLanguage] : 'en_EN';

}